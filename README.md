
[![Waffle.io - Columns and their card count](https://badge.waffle.io/fryntiz/torrentlibre.svg?columns=all)](https://waffle.io/fryntiz/torrentlibre)

# Torrent Libre

TorrentLibre es una aplicación desarrollada por
[Raúl Caro Pastorino](https://raupulus.dev) como proyecto final de un ciclo
superior DAW.

Esta aplicación tiene como objetivo compartir torrents para compartir
material libre o con licencias abiertas indicando la autoría del creador.

El repositorio para el desarrollo de esta aplicación es
[https://github.com/raupulus/torrentlibre](https://github.com/raupulus/torrentlibre)

# Documentación

Puedes ver la documentación en el siguiente enlace:
[https://raupulus.github.io/torrentlibre/](https://raupulus.github.io/torrentlibre/)

# Imágenes del proyecto

![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man1.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man2.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man3.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man4.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man5.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man6.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man7.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man8.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man9.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man10.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man11.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man12.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man13.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man14.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man15.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man16.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man17.png)
![Imagen de la aplicación](https://raw.githubusercontent.com/raupulus/torrentlibre/master/docs/images/man18.png)
